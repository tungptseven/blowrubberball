// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { SoundType } from "./AudioSourceControl"
import GameScene from "./GameScene"

const { ccclass, property } = cc._decorator
export enum GameStatus {
    Game_Ready = 0,
    Game_Playing,
    Game_Over
}

@ccclass
export default class GameLayout extends cc.Component {
    gameStatus: GameStatus = GameStatus.Game_Ready
    gameControl: GameScene = null
    homeLayout = null
    gameOverLayout = null
    ball: cc.Node
    ring: cc.Node
    passImg: cc.Node
    failImg: cc.Node
    explodeImg: cc.Node

    gameScore = 0
    ballScale = 0
    ringScale = 1
    onTouch = false
    isScored = false
    collisionManager = null

    @property(cc.Label)
    scoreLbl: cc.Label = null

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.collisionManager = cc.director.getCollisionManager()
        this.collisionManager.enabled = true

        this.ball = this.node.getChildByName('Ball').getChildByName('Ball_sprite')
        this.explodeImg = this.node.getChildByName('Ball').getChildByName('Explode_sprite')
        this.ring = this.node.getChildByName('Ring')
        this.passImg = this.node.getChildByName('passImg')
        this.failImg = this.node.getChildByName('failImg')
        this.ball.scale = 0
        this.resetRing()

        cc.Canvas.instance.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this)
        cc.Canvas.instance.node.on(cc.Node.EventType.TOUCH_END, this.onTouchStart, this)
        this.gameScore = 0
        this.scoreLbl.string = this.gameScore.toString()
        this.gameStatus = GameStatus.Game_Playing
        this.homeLayout = cc.Canvas.instance.node.getChildByName('HomeLayout')
        this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
        this.gameOverLayout = cc.Canvas.instance.node.getChildByName('GameOverLayout')
    }

    onPlay(isReplay: boolean = true) {
        this.gameStatus = GameStatus.Game_Playing
        this.gameScore = 0
    }

    gameOver() {
        this.node.stopAllActions()
        this.gameStatus = GameStatus.Game_Over
        this.node.active = false
        this.gameOverLayout.active = true
        this.gameOverLayout.getComponent('GameOverLayout').gameScore = this.gameScore
    }

    onBack() {
        // this.node.stopAllActions()
        this.gameStatus = GameStatus.Game_Over
        this.node.active = false
        this.homeLayout.active = true
    }

    onTouchStart(event) {
        if (event.type == 'touchstart') {
            this.onTouch = true
        } else {
            // this.ball.getComponent('BallControl').onDeath()
            this.explodeImg.active = true
            this.ball.active = false
            setTimeout(() => {
                this.onTouch = false
                this.explodeImg.active = false
                this.ball.active = true
            }, 300)
            this.resetRing()
            this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Die)
            if (this.isScored) {
                this.gameScore = this.gameScore + 10
                this.isScored = false
                this.passImg.active = true
                setTimeout(() => this.passImg.active = false, 300)
            } else {
                // this.ball.getComponent('BallControl').onDeath()
                // this.gameScore = 0
                this.failImg.active = true
                setTimeout(() => this.failImg.active = false, 300)
                this.gameOver()
            }
        }
    }

    resetRing() {
        let maxScale = 1.25
        let minScale = 0.3
        this.ringScale = minScale + Math.random() * (maxScale - minScale)
        return this.ring.scale = this.ringScale
    }

    start() {

    }


    update(dt) {
        if (this.gameStatus === GameStatus.Game_Playing) {
            if (this.onTouch) {
                this.ballScale += 0.01
                this.ball.scale = this.ballScale
            } else {
                this.ballScale = 0
                this.ball.scale = this.ballScale
            }
            this.scoreLbl.string = this.gameScore.toString()
        }
    }
}
