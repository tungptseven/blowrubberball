// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"

const { ccclass, property } = cc._decorator

@ccclass
export default class BallControl extends cc.Component {
    // gameControl: GameScene = null
    gameLayout: GameLayout = null

    @property(cc.Animation)
    anim: cc.Animation = null

    onCollisionEnter(other, self) {
        if (other.tag == 1) {
            this.gameLayout.isScored = true
            // this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Star)
        } else if (other.tag == 2) {
            this.gameLayout.isScored = false
        }
    }
    // LIFE-CYCLE CALLBACKS:

    onDeath() {
        this.anim.play(this.anim.getClips()[0].name)
    }

    onLoad() {
        // this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout').getComponent('GameLayout')

    }

    start() {

    }

    // update (dt) {}
}
